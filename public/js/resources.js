'use strict'

const resourceLocations = {
    element: [
        'road',
        'road_s',
        'road_e',
        'road_es',
        'road_n',
        'road_ns',
        'road_ne',
        'road_nes',
        'road_w',
        'road_ws',
        'road_we',
        'road_wes',
        'road_wn',
        'road_wns',
        'road_wne',
        'road_wnes',
        'flag',
        'tent',
        'shack',
        'well'

    ],
    button: [
        'road',
        'house',
        'well'
    ],
    stat: [
        'gold',
        'population',
        'date'
    ],
    sprite: [
        'settler'
    ],
    misc: [
        'shadow_corner'
    ]
}

const resources = {}

const loadResources = callback => {
    const promises = []
    Object.keys(resourceLocations).forEach(category => {
        resources[category] = {}

        resourceLocations[category].forEach(name => {
            promises.push(new Promise((resolve, reject) => {
                const image = new Image()
                image.onload = () => {
                    resources[category][name] = image
                    resolve()
                }

                image.src = `image/${category}/${name}.svg`
            }))
        })
    })

    Promise.all(promises).then(callback).catch(() => {
        console.log('ERROR WHILE LOADING RESOURCES');
    })
}
