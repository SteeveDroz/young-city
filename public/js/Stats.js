'use strict'

class Stats {
    static get MONTHS() {
        return ['jan', 'fév', 'mar', 'avr', 'mai', 'juin', 'juil', 'aou', 'sep', 'oct', 'nov', 'déc']
    }
    constructor(gold, date = '1910-01-01') {
        this.gold = gold
        this.population = 0
        this.date = new Date(date)

        this.height = 36
    }

    render(ctx, width) {
        const background = ctx.createLinearGradient(0, 36, 0, 32)
        background.addColorStop(0, '#fed')
        background.addColorStop(1, '#ba9')
        ctx.fillStyle = background

        ctx.fillRect(0, 0, width, this.height)

        ctx.fillStyle = '#000'

        ctx.drawImage(resources.stat.gold, 4, 4)
        ctx.fillText(this.gold, 32, 20)

        ctx.drawImage(resources.stat.population, 104, 4)
        ctx.fillText(this.population, 132, 20)

        ctx.drawImage(resources.stat.date, 204, 4)
        const year = this.date.getFullYear() < 2000 ? `${2000 - this.date.getFullYear()} av. J.-C.` : `${ this.date.getFullYear() - 1999} ap. J.-C.`
        ctx.fillText(`${this.date.getDate()} ${Stats.MONTHS[this.date.getMonth()]} ${year}`, 232, 20)
    }

    changeDate() {
        this.date = new Date(this.date.getTime() + 24 * 3600 * 1000)
    }

    setPopulation(population) {
        this.population = population
    }
}
