'use strict'

class Map {
    constructor(file, whenReady) {
        this.loaded = false
        this.readFileContent(`data/${file}.dat`, whenReady)
        this.tiles = []
        this.width = 0
        this.height = 0
        this.overlay = null
    }

    readFileContent(file, whenReady) {
        const xhr = new XMLHttpRequest()
        xhr.overrideMimeType('text/plain')
        xhr.open('GET', file, true)
        xhr.onreadystatechange = () => {
            if (xhr.readyState == 4 && xhr.status == '200') {
                const data = xhr.responseText

                const lines = data.split('\n').filter(line => line)

                this.width = lines[0].length / 2
                this.height = lines.length

                this.tiles = []

                for (let y = 0; y < lines.length; y++) {
                    for (let x = 0; x < lines[y].length; x += 2) {
                        this.tiles.push(new Tile(lines[y][x], lines[y][x + 1], x / 2, y))
                    }
                }

                this.loaded = true

                const tileIndex = Map.coordToId(this.height / 2, this.width / 2, this.width)

                const center = this.tileToRealCoord(tileIndex)

                whenReady(center)
            }
        }
        xhr.send(null)
    }

    getTile(x, y) {
        return this.tiles[this.width * y + x]
    }

    update() {
        this.tiles.forEach(tile => tile.update(this))
    }

    render(ctx) {
        ctx.save()

        const overlayMap = this.getOverlay(this.overlay)

        for (let i = 0; i < this.tiles.length; i++) {
            const {
                x,
                y
            } = Map.idToCoord(i, this.width)

            const neighbors = this.getNeighbors(x, y)

            this.tiles[i].render(ctx, x == this.width - 1, y == this.height - 1, neighbors, this.overlay, overlayMap[i])
        }

        ctx.restore()
    }

    getNeighbors(x, y) {
        const neighbors = {
            west: null,
            north: null,
            east: null,
            south: null
        }

        if (x > 0) {
            neighbors.west = this.getTile(x - 1, y)
        }
        if (y > 0) {
            neighbors.north = this.getTile(x, y - 1)
        }
        if (x < this.width - 1) {
            neighbors.east = this.getTile(x + 1, y)
        }
        if (y < this.height - 1) {
            neighbors.south = this.getTile(x, y + 1)
        }

        return neighbors
    }

    coordToTile(x, y) {
        const tileX = Math.floor(1 / 64 * x + 1 / 32 * (y + 16))
        const tileY = Math.floor(-1 / 64 * x + 1 / 32 * (y + 16))
        if (tileX < this.width && tileX >= 0) {
            return this.tiles[tileY * this.width + tileX]
        } else {
            return undefined
        }
    }

    tileToRealCoord(tile) {
        return this.tiles[tile].realCoord
    }

    removeHouses() {
        const allowedBuildings = this.getAllowedBuildings()

        for (let i = 0; i < this.tiles.length; i++) {
            if (this.tiles[i].element.type == 'h' && !allowedBuildings[i]) {
                this.tiles[i].element = new Empty()
            }
        }
    }

    getAllowedBuildings() {
        return this.getOverlay('r')
    }

    getOverlay(type) {
        const values = []
        for (let i = 0; i < this.tiles.length; i++) {
            values.push(0)
        }

        this.tiles.forEach((tile, i) => {
            if (tile.element.type == type) {
                const tilesInRange = Map.getTilesInRange(i, this.width, this.height, tile.element.range)
                tilesInRange.forEach(id => {
                    values[id] += 1
                })
            }
        })

        return values
    }

    get exits() {
        const exits = []
        for (let i = 0; i < this.tiles.length; i++) {
            if (Map.isOnBorder(i, this.width, this.height)) {
                if (this.tiles[i].element.type == 'r') {
                    exits.push(this.tiles[i])
                }
            }
        }
        return exits
    }

    get freeHomes() {
        return this.tiles
            .filter(tile => tile.element.type == 'h')
            .filter(tile => tile.element.place > 0)
    }

    static coordToId(x, y, width) {
        return Math.floor(y) * width + Math.floor(x)
    }

    static idToCoord(i, width) {
        return {
            x: i % width,
            y: Math.floor(i / width)
        }
    }

    static getTilesInRange(index, width, height, range) {
        const tiles = []
        const {
            x,
            y
        } = Map.idToCoord(index, width)

        const left = Math.max(x - range, 0)
        const right = Math.min(x + range, width - 1)
        const top = Math.max(y - range, 0)
        const bottom = Math.min(y + range, height - 1)

        for (let iY = top; iY <= bottom; iY++) {
            for (let iX = left; iX <= right; iX++) {
                tiles.push(iY * width + iX)
            }
        }

        return tiles
    }

    static isOnBorder(i, width, height) {
        const {
            x,
            y
        } = Map.idToCoord(i, width)

        if (x == 0) return true
        if (x == width - 1) return true
        if (y == 0) return true
        if (y == height - 1) return true
        return false
    }
}
