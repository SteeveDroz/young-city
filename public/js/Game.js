'use strict'

class Game {
    static get WIDTH() {
        return 1100
    }
    static get HEIGHT() {
        return 700
    }

    static get SECONDS_IN_DAY() {
        return 5
    }

    constructor(canvasId) {
        this.canvas = document.querySelector(`#${canvasId}`)
        this.ctx = this.canvas.getContext('2d')

        this.canvas.width = Game.WIDTH
        this.canvas.height = Game.HEIGHT

        this.fullscreen = false

        this.map = new Map('poc1', center => {
            this.center = center
        })

        this.people = []

        this.menu = new Menu()
        this.stats = new Stats(1000)

        this.zoom = 1

        this.center = {
            x: 0,
            y: 0
        }

        this.keys = {
            left: false,
            up: false,
            right: false,
            down: false
        }

        this.panStart = {
            x: null,
            y: null,
            center: null
        }

        this.panSpeed = 1

        this.frameRate = 24

        this.nextDay = this.framesInDay

        this.setEvents()
    }

    get framesInDay() {
        return Game.SECONDS_IN_DAY * this.frameRate
    }

    setEvents() {
        this.canvas.onmousemove = event => {
            this.mouseMove(event)
        }
        this.canvas.onmousedown = event => {
            this.mouseDown(event)
        }
        this.canvas.onmouseup = event => {
            this.mouseUp(event)
        }
        this.canvas.oncontextmenu = event => {
            event.preventDefault()
        }

        document.onkeydown = event => {
            this.keyDown(event)
        }
        document.onkeyup = event => {
            this.keyUp(event)
        }
    }

    static run(canvasId, callback = value => {}) {
        loadResources(() => {
            const game = new Game(canvasId)
            this.loop = setInterval(() => {
                game.update()
                game.render()
            }, 1000.0 / game.frameRate)
            callback(game)
        })
    }

    update() {
        const pan = this.pan()
        this.center.x += pan.x
        this.center.y += pan.y

        this.map.update()

        this.people.sort((a, b) => a.y - b.y)
        this.people.forEach(person => {
            person.update()
        })

        this.nextDay -= 1
        if (this.nextDay <= 0) {
            this.nextDay = this.framesInDay
            this.changeDate()
        }
    }

    render() {
        this.ctx.fillStyle = '#000000'
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height)

        this.ctx.save()
        this.ctx.scale(this.zoom, this.zoom)
        this.ctx.translate(this.canvas.width / 2 - this.center.x, this.canvas.height / 2 - this.center.y)

        this.map.render(this.ctx)

        this.people.forEach(person => {
            person.render(this.ctx)
        })

        this.ctx.restore()

        this.menu.render(this.ctx, this.canvas)
        this.stats.render(this.ctx, this.canvas.width - this.menu.width)
    }

    pan() {
        const delta = {
            x: 0,
            y: 0
        }

        if (this.isPressed('left', 'right')) {
            delta.x = -1
        } else if (this.isPressed('right', 'left')) {
            delta.x = 1
        }

        if (this.isPressed('up', 'down')) {
            delta.y = -1
        } else if (this.isPressed('down', 'up')) {
            delta.y = 1
        }

        return delta
    }

    changeDate() {
        this.stats.changeDate()
        this.map.removeHouses()

        const freeHomes = this.map.freeHomes
        const freePlaces = freeHomes.reduce((sum, home) => sum + home.element.place, 0)

        for (let i = 0; i < freeHomes.length; i++) {
            setTimeout(() => {
                if (freeHomes[i].element.place > 0) {
                    this.addSettler(freeHomes[i])
                    freeHomes[i].element.onTheirWay += 1
                }
            }, Math.random() * Game.SECONDS_IN_DAY * 1000)
        }

        this.stats.setPopulation(this.population)
    }

    addSettler(home) {
        const exits = this.map.exits
        exits.sort((a, b) => a.getDistance(home) - b.getDistance(home))
        const exit = exits[0]
        const path = Settler.getPath(exit, home, this.map)
        this.people.push(new Settler(path))
    }

    isPressed(pressed, notPressed) {
        return this.keys[pressed] && !this.keys[notPressed]
    }

    getCoordinates(event) {
        return {
            x: event.clientX - this.canvas.offsetLeft,
            y: event.clientY - this.canvas.offsetTop
        }
    }

    get population() {
        return this.map.tiles.filter(tile => tile.element.type == 'h').reduce((sum, tile) => sum + tile.element.occupation, 0)
    }

    mouseMove(event) {
        const {
            x,
            y
        } = this.getCoordinates(event)

        this.map.tiles.forEach(tile => {
            tile.hover = false
        })

        if ((event.buttons & 2) > 0 && this.panStart.x !== null && this.panStart.y !== null) {
            const coordinates = this.getCoordinates(event)

            const deltaX = coordinates.x - this.panStart.x
            const deltaY = coordinates.y - this.panStart.y

            this.center.x = this.panStart.center.x - deltaX
            this.center.y = this.panStart.center.y - deltaY
        }

        if (this.menu.contains(x, y, this.canvas)) {
            this.menu.mouseMove(x - this.canvas.width + this.menu.width, y)
        } else {
            const hoveredTile = this.map.coordToTile(x + this.center.x - this.canvas.width / 2, y + this.center.y - this.canvas.height / 2)
            if (hoveredTile !== undefined) {
                hoveredTile.hover = true
            }
        }
    }

    mouseDown(event) {
        if (event.button == 2) {
            const coordinates = this.getCoordinates(event)
            if (!this.menu.contains(coordinates.x, coordinates.y, this.canvas)) {
                this.panStart.x = coordinates.x
                this.panStart.y = coordinates.y

                this.panStart.center = {
                    x: this.center.x,
                    y: this.center.y
                }
            }
        }
    }

    mouseUp(event) {
        const {
            x,
            y
        } = this.getCoordinates(event)

        if (event.button == 0) {
            if (this.menu.contains(x, y, this.canvas)) {
                this.menu.mouseUp(x - this.canvas.width + this.menu.width, y)

                if (this.menu.active !== null) {
                    if (this.menu.active.name == 'well') {
                        this.map.overlay = 'w'
                    } else if (this.menu.active.name == 'house') {
                        this.map.overlay = 'r'
                    } else {
                        this.map.overlay = null
                    }
                } else {
                    this.map.overlay = null
                }

            } else {
                const tile = this.map.coordToTile(x + this.center.x - this.canvas.width / 2, y + this.center.y - this.canvas.height / 2)
                if (this.menu.active) this.menu.active.execute(tile, this)
            }
        } else if (event.button == 2) {
            this.panStart.x = null
            this.panStart.y = null
            this.panStart.center = null
        }
    }

    keyDown(event) {
        this.keyChange(event, true)
    }

    keyUp(event) {
        this.keyChange(event, false)
    }

    keyChange(event, state) {
        switch (event.keyCode) {
            case 37:
                this.keys.left = state
                break

            case 38:
                this.keys.up = state
                break

            case 39:
                this.keys.right = state
                break

            case 40:
                this.keys.down = state
                break

            case 70:
                if (state == false) {
                    document.onmozfullscreenchange = () => {
                        if (this.fullscreen) {
                            this.canvas.width = Game.WIDTH
                            this.canvas.height = Game.HEIGHT
                        } else {
                            this.canvas.width = window.innerWidth
                            this.canvas.height = window.innerHeight
                        }
                        this.fullscreen ^= true
                    }

                    if (this.fullscreen) {
                        document.mozCancelFullScreen()
                    } else {
                        this.canvas.mozRequestFullScreen()
                    }
                }
                break
        }
    }
}
