'use strict'

class Person {

    constructor(tile, speed, spritesheet, steps, directions) {
        this.x = tile.realCoord.x
        this.y = tile.realCoord.y
        this.speed = speed
        this.sprite = new Sprite(spritesheet, steps, directions)
        this.step = 0
        this.direction = 0
    }

    get steps() {
        return this.sprite.width
    }

    get directions() {
        return this.sprite.height
    }

    static getPath(start, end, map) {
        const data = []
        map.tiles.forEach(tile => {
            data.push({
                tile: tile,
                cost: Infinity,
                heuristic: Infinity,
                parent: null
            })
        })

        const compareData = (a, b) => data[a].heuristic - data[b].heuristic
        const distance = (a, b) => Math.sqrt((b.tile.coord.x - a.tile.coord.x) * (b.tile.coord.x - a.tile.coord.x) + (b.tile.coord.y - a.tile.coord.y) * (b.tile.coord.y - a.tile.coord.y))

        const closed = []
        const open = []

        const startId = Map.coordToId(start.coord.x, start.coord.y, map.width)
        const endId = Map.coordToId(end.coord.x, end.coord.y, map.width)

        data[startId].heuristic = distance(data[startId], data[endId])
        data[startId].cost = 0

        open.push(startId)

        while (open.length > 0) {
            open.sort(compareData)
            const datumId = open.shift()
            const datum = data[datumId]
            if (datum.tile == end) {
                const path = []
                let current = datum
                do {
                    path.unshift(current.tile)
                    current = current.parent
                }
                while (current != null)
                return path
            }
            const neighbors = Object.values(map.getNeighbors(datum.tile.coord.x, datum.tile.coord.y))

            Object.keys(neighbors)
                .filter(id => neighbors[id] != null)
                .filter(id => this.canWalk(neighbors[id]))
                .forEach(id => {
                    const i = Map.coordToId(neighbors[id].coord.x, neighbors[id].coord.y, map.width)
                    if (closed.includes(i)) {
                        return
                    } else if (compareData(i, datumId) > 0) {
                        data[i].cost = datum.cost + 1
                        data[i].heuristic = data[i].cost + distance(data[i], data[Map.coordToId(end.coord.x, end.coord.y, map.width)])
                        data[i].parent = datum
                        open.push(i)
                    }
                })
            closed.push(datumId)
        }

        return [start, end]
    }
}
