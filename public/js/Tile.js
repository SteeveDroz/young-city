'use strict'

class Tile {
    static get SIZE() {
        return 32
    }

    constructor(type, element, x, y) {
        this.type = type
        this.element = Element.createElement(element)
        this.coord = {
            x: x,
            y: y
        }
        this.hover = false
    }

    update(map) {
        this.element.update(this, map)
    }

    render(ctx, isLastX, isLastY, neighbors, overlay = null, overlayValue = 0) {
        ctx.save()
        ctx.translate(this.coord.x * 32 - this.coord.y * 32, this.coord.x * 16 + this.coord.y * 16)

        let overlayColor
        switch (overlay) {
            case 'w':
                overlayColor = `rgba(0, 0, 255, ${Math.min(overlayValue * 0.5, 0.5)})`
                break

            case 'r':
                overlayColor = `rgba(255, 0, 0, ${Math.min(overlayValue * 0.5, 0.5)})`
                break

            default:
                overlayColor = '#fff0'
        }

        ctx.beginPath()
        ctx.moveTo(0, -Tile.SIZE / 2)
        ctx.lineTo(Tile.SIZE, 0)
        ctx.lineTo(0, Tile.SIZE / 2)
        ctx.lineTo(-Tile.SIZE, 0)
        ctx.fillStyle = this.color
        ctx.fill()
        ctx.strokeStyle = overlayColor
        ctx.lineWidth = 10
        ctx.stroke()

        if (isLastX) {
            ctx.beginPath()
            ctx.moveTo(0, Tile.SIZE / 2)
            ctx.lineTo(0, Tile.SIZE)
            ctx.lineTo(Tile.SIZE, Tile.SIZE / 2)
            ctx.lineTo(Tile.SIZE, 0)
            ctx.fillStyle = this.lighten(this.color, -20)
            ctx.fill()
        }

        if (isLastY) {
            ctx.beginPath()
            ctx.moveTo(-Tile.SIZE, 0)
            ctx.lineTo(-Tile.SIZE, Tile.SIZE / 2)
            ctx.lineTo(0, Tile.SIZE)
            ctx.lineTo(0, Tile.SIZE / 2)
            ctx.fillStyle = this.lighten(this.color, 20)
            ctx.fill()
        }

        if (this.hover) {
            ctx.beginPath()
            ctx.moveTo(0, -Tile.SIZE / 2)
            ctx.lineTo(Tile.SIZE, 0)
            ctx.lineTo(0, Tile.SIZE / 2)
            ctx.lineTo(-Tile.SIZE, 0)
            ctx.fillStyle = this.lighten(this.color, 50)
            ctx.fill()
        }

        const neighborElements = Object.keys(neighbors).reduce((carry, direction) => {
            carry[direction] = neighbors[direction] !== null ? neighbors[direction].element : null
            return carry
        }, {})

        this.element.render(ctx, neighborElements)

        ctx.restore()
    }

    canBuild() {
        if (this.type == 'w') return false
        if (this.type == 's') return false
        return this.element.type == '_'
    }

    setElement(type) {
        this.element = new Element(type)
    }

    getDistance(other) {
        return Math.sqrt((other.coord.x - this.coord.x) * (other.coord.x - this.coord.x) + (other.coord.y - this.coord.y) * (other.coord.y - this.coord.y))
    }

    get color() {
        switch (this.type) {
            case 'g':
                return '#008800'

            case 'r':
                return '#444444'

            case 's':
                return '#888888'

            case 'w':
                return '#0000aa'

            case 't':
                return '#004400'

            case 'f':
                return '#aaaa44'
        }
    }

    get realCoord() {
        return {
            x: 32 * this.coord.x - 32 * this.coord.y,
            y: 16 * this.coord.x + 16 * this.coord.y
        }
    }

    lighten(color, amount) {
        const value = parseInt(color.slice(1), Tile.SIZE / 2)

        const r = Math.min(Math.max((value >> Tile.SIZE / 2) + amount, 0), 255)
        const g = Math.min(Math.max(((value >> 8) & 0x00ff) + amount, 0), 255)
        const b = Math.min(Math.max((value & 0x0000ff) + amount, 0), 255)

        const newValue = (r << Tile.SIZE / 2) | (g << 8) | b

        const stringValue = ('000000' + newValue.toString(Tile.SIZE / 2)).slice(-6)

        return `#${stringValue}`
    }
}
