'use strict'

class Settler extends Person {
    static get TICKS_IN_STEP() {
        return 5
    }
    constructor(path) {
        super(path[0], 1.3, resources.sprite.settler, 4, 4)

        this.target = path
        this.step = 0
        this.direction = 0
        this.ticks = Settler.TICKS_IN_STEP
    }

    update() {
        if (--this.ticks <= 0) {
            this.step++
            this.ticks = Settler.TICKS_IN_STEP
        }
        if (this.displayable) {
            const {
                x,
                y
            } = this.target[0].realCoord

            const direction = this.getDirection(this.target[0])
            this.x += direction.x * this.speed
            this.y += direction.y * this.speed

            this.faceDirection(direction)

            if ((this.x - x) * (this.x - x) + (this.y - y) * (this.y - y) < 4) {
                if (this.target.length > 1) {
                    this.target.shift()
                } else {
                    this.target = this.target[0]
                    this.target.element.occupation += 1
                    this.target.element.onTheirWay -= 1
                }
            }
        }
    }

    render(ctx) {
        if (this.displayable) {
            ctx.save()
            ctx.translate(-this.sprite.tileWidth / 2, -this.sprite.tileHeight)

            this.sprite.draw(ctx, this.step % this.steps, this.direction % this.directions, this.x, this.y)

            ctx.restore()
        }
    }

    faceDirection(direction) {
        this.direction = (direction.x < 0 ? 1 : 0) + (direction.y < 0 ? 2 : 0)
    }

    get displayable() {
        return Array.isArray(this.target)
    }

    getDirection(target) {
        if (this.displayable) {
            const angle = Math.atan2(target.realCoord.y - this.y, target.realCoord.x - this.x)
            return {
                x: Math.cos(angle) * this.speed,
                y: Math.sin(angle) * this.speed
            }
        } else {
            return {
                x: 0,
                y: 0
            }
        }
    }

    static canWalk(tile) {
        if (tile.type == 'w') return false
        if (tile.type == 's') return false
        return true
    }
}
