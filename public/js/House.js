'use string'

class House extends Element {
    constructor() {
        super('h')
        this.level = 0
        this.goalLevel = 0
        this.occupation = 0
        this.onTheirWay = 0
        this.range = 0
    }

    update(tile, map) {
        this.goalLevel = this.level + Math.sign(this.getNextLevel(tile, map) - this.level)
        if (this.occupation == this.goalLevel * this.goalLevel) {
            this.level = this.goalLevel
        }
    }

    render(ctx, neighbors) {
        ctx.save()
        this.translate(ctx)

        ctx.drawImage(this.image, 0, 0)

        ctx.restore()
    }

    getNextLevel(tile, map) {
        const water = map.getOverlay('w')
        const i = Map.coordToId(tile.coord.x, tile.coord.y, map.width)
        if (water[i] > 0) {
            return 2
        }
        return 1
    }

    translate(ctx) {
        switch (this.level) {
            case 0:
                ctx.translate(-32, -16)
                break

            case 1:
                ctx.translate(-32, -32)
                break

            case 2:
                ctx.translate(-32, -48)
                break
        }
    }

    get image() {
        switch (this.level) {
            case 0:
                return resources.element.flag
                break

            case 1:
                return resources.element.tent
                break

            case 2:
                return resources.element.shack
                break
        }
    }

    get place() {
        return this.goalLevel * this.goalLevel - this.occupation - this.onTheirWay
    }
}
