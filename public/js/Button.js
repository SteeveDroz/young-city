'use strict'

class Button {
    constructor(name, image, action) {
        this.name = name
        this.image = image
        this.action = action
        this.width = 50
        this.height = 40
        this.active = false
        this.hover = false
    }

    static getRoad() {
        return new Button('road', resources.button.road, (tile, game) => {
            if (this.canBuild(tile, game, 10)) {
                game.stats.gold -= 10
                tile.element = new Road()
            }
        })
    }

    static getHouse() {
        return new Button('house', resources.button.house, (tile, game) => {
            if (this.canBuild(tile, game)) {
                tile.element = new House()
            }
        })
    }

    static getWell() {
        return new Button('well', resources.button.well, (tile, game) => {
            if (this.canBuild(tile, game, 100)) {
                game.stats.gold -= 100
                tile.element = new Well()
            }
        })
    }

    render(ctx) {
        ctx.save()
        ctx.translate(this.x, this.y)

        ctx.fillStyle = this.active ? 'white' : this.hover ? 'gray' : 'transparent'
        ctx.fillRect(0, 0, this.width, this.height)
        ctx.drawImage(this.image, 0, 0)

        ctx.restore()
    }

    execute(tile, game) {
        this.action(tile, game)
    }

    contains(x, y) {
        if (x < this.x) return false
        if (y < this.y) return false
        if (x > this.x + this.width) return false
        if (y > this.y + this.height) return false
        return true
    }

    static canBuild(tile, game, gold = -Infinity) {
        return tile !== undefined && tile.canBuild() && game.stats.gold >= gold
    }
}
