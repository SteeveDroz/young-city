'use strict'

class Sprite {
    constructor(spritesheet, width, height) {
        this.spritesheet = spritesheet
        this.width = width
        this.height = height

        this.tileWidth = spritesheet.width / width
        this.tileHeight = spritesheet.height / height
    }

    draw(ctx, idX, idY, x, y) {
        ctx.drawImage(this.spritesheet, idX * this.tileWidth, idY * this.tileHeight, this.tileWidth, this.tileHeight, x, y, this.tileWidth, this.tileHeight)
    }
}
