'use strict'

class Road extends Element {
    constructor() {
        super('r')
        this.range = 2
    }

    render(ctx, neighbors) {
        ctx.save()
        ctx.translate(-32, -16)
        let connectors = ''
        if (neighbors.west == null || neighbors.west.type == 'r') {
            connectors += 'w'
        }
        if (neighbors.north == null || neighbors.north.type == 'r') {
            connectors += 'n'
        }
        if (neighbors.east == null || neighbors.east.type == 'r') {
            connectors += 'e'
        }
        if (neighbors.south == null || neighbors.south.type == 'r') {
            connectors += 's'
        }

        const imageName = connectors == '' ? 'road' : `road_${connectors}`

        ctx.fillStyle = '#ffffff'
        ctx.drawImage(resources.element[imageName], 0, 0)

        ctx.restore()
    }
}
