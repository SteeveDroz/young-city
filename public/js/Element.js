'use strict'

class Element {
    constructor(type) {
        this.type = type
    }

    update(tile, map) {}

    render(ctx, neighbors) {
        throw new Error('This method must be implemented')
    }

    static createElement(type) {
        switch (type) {
            case 'r':
                return new Road()
                break

            default:
                return new Empty()
                break
        }
    }
}
