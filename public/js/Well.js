'use strict'

class Well extends Element {
    constructor() {
        super('w')
        this.range = 4
    }

    update(tile, map) {}

    render(ctx, neighbors) {
        ctx.save()
        ctx.translate(-16, -20)

        ctx.drawImage(resources.element.well, 0, 0)

        ctx.restore()
    }
}
