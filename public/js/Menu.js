'use strict'

class Menu {
    constructor() {
        this.width = 150
        this.buttons = [Button.getRoad(), Button.getHouse(), Button.getWell()]

        this.placeButtons()
    }

    placeButtons() {
        this.buttonGap = 10
        this.borderPadding = 30

        for (let i = 0; i < this.buttons.length; i++) {

            this.buttons[i].x = (i % 2) * (this.buttons[i].width + this.buttonGap) + this.borderPadding

            this.buttons[i].y = Math.floor(i / 2) * (this.buttons[i].height + this.buttonGap) + this.buttonGap
        }
    }

    render(ctx, game) {
        ctx.save()
        ctx.translate(game.width - this.width, 0)

        const background = ctx.createLinearGradient(0, 0, 20, 0)
        background.addColorStop(0, '#fed')
        background.addColorStop(1, '#ba9')
        ctx.fillStyle = background
        ctx.fillRect(0, 0, this.width, game.height)

        ctx.fillStyle = '#ba9'
        ctx.fillRect(0, 0, this.width, 36)

        ctx.drawImage(resources.misc.shadow_corner, 0, 32)

        this.buttons.forEach(button => {
            button.render(ctx)
        })

        ctx.restore()
    }

    get active() {
        let active = null
        this.buttons.forEach(button => {
            if (button.active) {
                active = button
            }
        })
        return active
    }

    contains(x, y, game) {
        return x >= game.width - this.width
    }

    mouseMove(x, y) {
        this.buttons.forEach(button => {
            button.hover = button.contains(x, y)
        })

    }

    mouseUp(x, y) {
        this.buttons.forEach(button => {
            button.active = button.contains(x, y)
        })
    }
}
