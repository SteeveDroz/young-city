'use strict'

class Empty extends Element {
    constructor() {
        super('_')
    }

    render(ctx, neighbors) {}
}
