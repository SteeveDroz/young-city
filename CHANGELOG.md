# Changelog

## [v0.1.0-alpha]

- A map is displayed.
- The user can pan around the map with keyboard arrows.
- Fullscreen mode can be enabled and disabled by pressing `F` when using Firefox.

[v0.1.0-alpha]: https://gitlab.com/SteeveDroz/young-city/compare/v0.0.0-alpha...v0.1.0-alpha
