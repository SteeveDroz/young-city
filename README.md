# Young City

Young City is a RTS game where you build a city out of nothing.

Yeah, I know what you think: yet another one! To which I answer: and why not?

The game is still in early stage, but stay tuned for updates!

[Play it](https://steevedroz.gitlab.io/young-city)
